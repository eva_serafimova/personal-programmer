<?php

namespace App\Http\Constants;

class HTTPMessages
{
    const FORBIDDEN = 'resource forbidden';
    const NOT_FOUND = 'resource not found';
    const BAD_REQUEST = 'bad request';
}
