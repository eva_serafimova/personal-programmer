<?php

namespace App\Http\Constants;

class HTTPCodes
{
    const FORBIDDEN = 403;
    const NOT_FOUND = 404;
    const BAD_REQUEST = 400;

    const SUCCESS = 200;
    const ACCEPTED = 202;
    const NO_CONTENT = 204;
}
