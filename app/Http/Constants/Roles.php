<?php

namespace App\Http\Constants;

class Roles
{
    const REGULAR = 'regular';
    const ADMINISTRATOR = 'administrator';
}
