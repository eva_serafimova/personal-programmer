<?php

namespace App\Http\Controllers;

use App\Http\Constants\HTTPCodes;
use Illuminate\Http\JsonResponse;
use App\Http\Services\UserService;
use App\Http\Resources\UserResource;

class UserController extends Controller
{
    public function __construct(
        private UserService $userService
    ) {}

    public function show(): JsonResponse
    {
        return new JsonResponse(
            data: new UserResource($this->userService->show(auth()->user())),
            status: HTTPCodes::SUCCESS
        );
    }
}
