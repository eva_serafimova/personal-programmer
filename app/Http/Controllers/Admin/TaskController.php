<?php

namespace App\Http\Controllers\Admin;

use App\DTO\TaskDTO;
use App\Http\Constants\HTTPCodes;
use App\Http\Requests\TaskRequest;
use App\Http\Controllers\Controller;
use App\Http\Resources\TaskResource;
use Illuminate\Http\{Request, JsonResponse};
use App\Http\Services\{TaskService, ApiProblemHandler};

class TaskController extends Controller
{
    public function __construct(
        private TaskService $taskService,
        private ApiProblemHandler $apiProblemHandler
    ) {}

    public function show(int $id): JsonResponse
    {
        $task = $this->taskService->getById($id);

        if (!$task) {
            return $this->apiProblemHandler->resourceNotFoundResponse();
        }

        return new JsonResponse(
            data: new TaskResource($task)
        );
    }

    public function index(Request $request): JsonResponse
    {
        return new JsonResponse(
            data: $this->taskService->list(null, $request->query->all())
        );
    }

    public function update(TaskRequest $request, int $id): JsonResponse
    {
        $task = $this->taskService->getById($id);

        if (!$task) {
            return $this->apiProblemHandler->resourceNotFoundResponse();
        }
        $taskDTO = TaskDTO::fromRequest($request);

        $this->taskService->update($task, $taskDTO);

        return new JsonResponse(
            data: new TaskResource($task),
            status: HTTPCodes::ACCEPTED
        );
    }

    public function delete(int $id): JsonResponse
    {
        $task = $this->taskService->getById($id);

        if (!$task) {
            return $this->apiProblemHandler->resourceNotFoundResponse();
        }

        $this->taskService->delete($task);

        return new JsonResponse(
            data: null, 
            status: HTTPCodes::NO_CONTENT
        );
    }
}
