<?php

namespace App\Http\Controllers\Regular;

use App\DTO\TaskDTO;
use App\Http\Constants\HTTPCodes;
use App\Http\Requests\TaskRequest;
use App\Http\Controllers\Controller;
use App\Http\Resources\TaskResource;
use Illuminate\Http\{Request, JsonResponse};
use App\Http\Services\{TaskService, ApiProblemHandler};

class TaskController extends Controller
{
    public function __construct(
        private TaskService $taskService,
        private ApiProblemHandler $apiProblemHandler
    ) {}

    public function show(int $id): JsonResponse
    {
        $task = $this->taskService->getById($id);

        if (!$task) {
            return $this->apiProblemHandler->resourceNotFoundResponse();
        }

        if ($task->user_id !== auth()->user()->id) {
            return $this->apiProblemHandler->resourceForbiddenResponse();
        }

        return new JsonResponse(
            data: new TaskResource($task)
        );
    }

    public function index(Request $request): JsonResponse
    {
        return new JsonResponse(
            data: $this->taskService->list(auth()->user(), $request->query->all())
        );
    }

    public function create(TaskRequest $request): JsonResponse
    {
        $taskDTO = TaskDTO::fromRequest($request);

        $task = $this->taskService->create($taskDTO);

        return new JsonResponse(
            data: new TaskResource($task)
        );
    }

    public function update(TaskRequest $request, int $id): JsonResponse
    {
        $task = $this->taskService->getById($id);

        if (!$task) {
            return $this->apiProblemHandler->resourceNotFoundResponse();
        }

        if ($task->user_id !== auth()->user()->id) {
            return $this->apiProblemHandler->resourceForbiddenResponse();
        }

        $taskDTO = TaskDTO::fromRequest($request);

        $this->taskService->update($task, $taskDTO);

        return new JsonResponse(
            data: new TaskResource($task),
            status: HTTPCodes::ACCEPTED
        );
    }

    public function delete(int $id): JsonResponse
    {
        $task = $this->taskService->getById($id);

        if (!$task) {
            return $this->apiProblemHandler->resourceNotFoundResponse();
        }

        if ($task->user_id !== auth()->user()->id) {
            return $this->apiProblemHandler->resourceForbiddenResponse();
        }

        $this->taskService->delete($task);

        return new JsonResponse(
            data: null, 
            status: HTTPCodes::NO_CONTENT
        );
    }
}
