<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class TaskResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'user' => new UserResource($this->user),
            'title' => $this->title,
            'description' => $this->description,
            'due_date' => $this->due_date ? $this->due_date->setTimezone(new \DateTimeZone('CET'))->format('Y-m-d H:i:s') : null,
            'status' => $this->status,
            'created_at' => $this->created_at?->format('Y-m-d H:i:s')
        ];
    }
}
