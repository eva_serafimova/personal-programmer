<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class Role
{

    /**
     * @param Request $request
     * @param Closure $next
     * @param mixed ...$roles
     * 
     * @return Response
     */
    public function handle(Request $request, Closure $next, $role): Response
    {
        $userRole = $request->user()->role->name;

        if ($userRole === strtoupper($role)) {
            return $next($request);
        }

        return redirect()->back();
    }
}
