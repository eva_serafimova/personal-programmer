<?php

namespace App\Http\Services;

use App\Http\Constants\{
    HTTPCodes, 
    HTTPMessages
};
use Illuminate\Http\JsonResponse;

class ApiProblemHandler
{
    /**
     * @param string $message
     * 
     * @return JsonResponse
     */
    public function resourceForbiddenResponse(): JsonResponse
    {
        return new JsonResponse(HTTPMessages::FORBIDDEN, HTTPCodes::FORBIDDEN);
    }

    /**
     * @param string $message
     * 
     * @return JsonResponse
     */
    public function resourceNotFoundResponse(): JsonResponse
    {
        return new JsonResponse(HTTPMessages::NOT_FOUND, HTTPCodes::NOT_FOUND);
    }

    /**
     * @return JsonResponse
     */
    public function generalBadRequestResponse(): JsonResponse
    {
        return new JsonResponse(HTTPMessages::BAD_REQUEST, HTTPCodes::BAD_REQUEST);
    }
}
