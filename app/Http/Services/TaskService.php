<?php

namespace App\Http\Services;

use App\DTO\TaskDTO;
use App\Models\{Task, User};
use App\Http\Resources\TaskResource;
use App\Interfaces\TaskRepositoryInterface;

class TaskService
{
    public function __construct(
        private TaskRepositoryInterface $taskRepository
    ) {
    }

    /**
     * @param int $id
     * 
     * @return Task|null
     */
    public function getById(int $id): ?Task
    {
        return $this->taskRepository->getById($id);
    }

    /**
     * @param TaskDTO $taskDTO
     * 
     * @return Task
     */
    public function create(TaskDTO $taskDTO): Task
    {

        $task = new Task;

        foreach ($taskDTO as $property => $value) {
            $task->{$property} = $value;
        }

        $this->taskRepository->save($task);

        return $task;
    }

    /**
     * @param Task $task
     * @param TaskDTO $taskDTO
     * 
     * @return Task
     */
    public function update(Task $task, TaskDTO $taskDTO): Task
    {
        foreach ($taskDTO as $property => $value) {
            if ($property !== 'user_id') {
                $task->{$property} = $value;
            }
        }

        $this->taskRepository->save($task);

        return $task;
    }

    /**
     * @param User|null $user
     * @param array $params
     * 
     * @return array
     */
    public function list(?User $user = null, array $params): array
    {
        $tasks = $this->taskRepository->listForUser($user, $params);

        return TaskResource::collection($tasks)->response()->getData(true);
    }

    /**
     * @param Task $task
     *
     * @return void
     */
    public function delete(Task $task): void
    {
        $this->taskRepository->delete($task);
    }
}
