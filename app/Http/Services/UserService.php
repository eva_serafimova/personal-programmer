<?php

namespace App\Http\Services;

use App\Models\User;
use App\Interfaces\UserRepositoryInterface;

class UserService
{
    public function __construct(
        private UserRepositoryInterface $userRepository
    ) {}

    /**
     * @param User $user
     * 
     * @return User
     */
    public function show(User $user): User
    {
        return $this->userRepository->getById($user->id);
    }
}