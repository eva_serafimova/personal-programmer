<?php

namespace App\Console\Commands;

use App\Models\Task;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;
use App\Notifications\TaskDueDateNotification;

class TaskDueDateEmailsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'emails:task-due-date-notification';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send an emails to users when their tasks are due';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $counter = 1;

        while ($tasks = Task::where('due_notification', false)
            ->where('due_date', '<=', new \DateTime('now'))
            ->limit(50)
            ->get()
        ) {
            if (!count($tasks)) {
                break;
            }

            foreach ($tasks as $task) {
                if (Mail::to($task->user->email)->send(new TaskDueDateNotification($task->title))) {
                    $this->line('mail sent succesfully to: ' . $task->user->email);

                    $task->due_notification = true;
                    $task->save();
                }
            }

            $counter++;
        }
    }
}
