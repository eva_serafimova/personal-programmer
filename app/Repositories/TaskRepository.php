<?php

namespace App\Repositories;

use App\Models\{Task, User};
use App\Interfaces\TaskRepositoryInterface;
use DateTimeZone;
use Illuminate\Pagination\LengthAwarePaginator;

class TaskRepository implements TaskRepositoryInterface 
{
    /**
     * @param int $id
     * 
     * @return Task|null
     */
    public function getById(int $id): ?Task
    {
        return Task::where('id', $id)->first();
    }

    /**
     * @param User|null $user
     * @param array $params
     * 
     * @return LengthAwarePaginator
     */
    public function listForUser(?User $user = null, array $params): LengthAwarePaginator
    {
        $filters = isset($params['filters']) ? $params['filters'] : [];
        $qb = Task::select();
        
        if ($user) {
            $qb->where('user_id', $user->id);
        }

        if (
            isset($filters['status']) 
            && in_array($filters['status'], ['in progress', 'to do', 'completed'])
        ) {
            $qb->where('status', $filters['status']);
        }

        if (
            isset($filters['range']) 
            && is_array($filters['range'])
            && count($filters['range']) === 2
            && strtotime($filters['range'][0])
            && strtotime($filters['range'][1])
        ) {
            $qb->where([
                ['due_date', '>=', (new \DateTime($filters['range'][0], new \DateTimeZone('CET')))->format('Y-m-d H:i:s')],
                ['due_date', '<=', (new \DateTime($filters['range'][1], new \DateTimeZone('CET')))->format('Y-m-d H:i:s')]
            ]);
        }
            
        $task = $qb->paginate(5);

        return $task;
    }

    /**
     * @param Task $task
     * 
     * @return Task
     */
    public function save(Task $task): Task
    {
        $task->save();

        return $task;
    }

    /**
     * @param Task $task
     * 
     * @return void
     */
    public function delete(Task $task): void
    {
        $task->delete();
    }
}