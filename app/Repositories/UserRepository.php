<?php

namespace App\Repositories;

use App\Interfaces\UserRepositoryInterface;
use App\Models\User;

class UserRepository implements UserRepositoryInterface 
{
    /**
     * @param int $id
     * 
     * @return User|null
     */
    public function getById(int $id): ?User
    {
        return User::with('role')->where('id', $id)->first();
    }
}