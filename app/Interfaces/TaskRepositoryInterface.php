<?php

namespace App\Interfaces;

use App\Models\{Task, User};
use Illuminate\Pagination\LengthAwarePaginator;

interface TaskRepositoryInterface 
{
    public function getById(int $id): ?Task;

    public function save(Task $task): Task;

    public function listForUser(?User $user, array $params): LengthAwarePaginator;

    public function delete(Task $task): void;
}