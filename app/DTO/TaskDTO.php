<?php

namespace App\DTO;

use Carbon\Carbon;
use App\Http\Requests\TaskRequest;

class TaskDTO
{
    public function __construct(
        public int $user_id,
        public string $title,
        public string $description,
        public \DateTime $due_date,
        public string $status,
        public \DateTime $created_at
    ) {}

    /**
     * @param TaskRequest $request
     * 
     * @return TaskDTO
     */
    public static function fromRequest(TaskRequest $request): TaskDTO
    {
        return new self(
            user_id: auth()->user()->id,
            title: $request->input('title'),
            description: $request->input('description'),
            due_date: Carbon::parse($request->input('due_date')),
            status: $request->input('status'),
            created_at: Carbon::now('CET')
        );
    }
}