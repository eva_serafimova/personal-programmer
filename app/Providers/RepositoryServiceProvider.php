<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Repositories\{TaskRepository, UserRepository};
use App\Interfaces\{TaskRepositoryInterface, UserRepositoryInterface};

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register(): void
    {
        $this->app->bind(UserRepositoryInterface::class, UserRepository::class);
        $this->app->bind(TaskRepositoryInterface::class, TaskRepository::class);
    }

    /**
     * Bootstrap services.
     */
    public function boot(): void
    {
        //
    }
}
