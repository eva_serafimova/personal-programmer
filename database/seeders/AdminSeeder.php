<?php

namespace Database\Seeders;

use App\Models\{User, Role};
use Illuminate\Database\Seeder;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        User::factory()->create([
            'email' => 'test@example.com',
            'role_id' => Role::where('name', 'ADMINISTRATOR')->first()->id
        ]);
    }
}
