<?php

use App\Http\Constants\Roles;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\Regular\TaskController;
use App\Http\Controllers\Admin\TaskController as AdminTaskController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->group(function () {
    Route::prefix('/user')->group(function () {
        Route::get('/', [UserController::class, 'show']);
    });

    Route::prefix(strtolower(Roles::REGULAR))->middleware('role:' . Roles::REGULAR)->group(function () {
        Route::prefix('tasks')->group(function () {
            Route::post('/', [TaskController::class, 'create']);
            Route::put('/{id}', [TaskController::class, 'update']);
            Route::get('/{id}', [TaskController::class, 'show']);
            Route::get('/', [TaskController::class, 'index']);
            Route::delete('/{id}', [TaskController::class, 'delete']);
        });
    });

    Route::prefix(strtolower(Roles::ADMINISTRATOR))->middleware('role:' . Roles::ADMINISTRATOR)->group(function () {
        Route::prefix('tasks')->group(function () {
            Route::put('/{id}', [AdminTaskController::class, 'update']);
            Route::get('/{id}', [AdminTaskController::class, 'show']);
            Route::get('/', [AdminTaskController::class, 'index']);
            Route::delete('/{id}', [AdminTaskController::class, 'delete']);
        });
    });
});
