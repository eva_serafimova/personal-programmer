<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Due Task</title>
</head>
<body>
    <div class="container m-5 p-5">
        Your task {{ $title }} is due
    </div>
</body>
</html>