import './bootstrap';
import { createApp } from 'vue';
import AppComponent from './App.vue';
import router from './router/index';
import axios from 'axios';

axios.defaults.baseURL = 'http://personal-programmer-app.test';
axios.defaults.withCredentials = true;

const app = createApp({
    components: {
        AppComponent
    }
});

app.use(router);
app.mount('#app');