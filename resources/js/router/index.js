import { createRouter, createWebHistory } from 'vue-router';
import ListTasks from '../components/regular/tasks/List.vue';
import ShowTask from '../components/regular/tasks/Show.vue';
import CreateTask from '../components/regular/tasks/Create.vue';
import Login from '../components/auth/Login.vue';
import Register from '../components/auth/Register.vue';

const routes = [
    {
        path: '/auth/login',
        name: 'login',
        component: Login,
        meta: {
            middlewares: ['GUEST'],
        }
    },
    {
        path: '/auth/register',
        name: 'register',
        component: Register,
        meta: {
            middlewares: ['GUEST'],
        }
    },
    {
        path: '/regular/tasks',
        name: 'regular/tasks',
        component: ListTasks,
        meta: {
            middlewares: ['AUTH', 'REGULAR'],
        }
    },
    {
        path: '/regular/tasks/:id',
        name: 'regular/tasks/details',
        component: ShowTask,
        meta: {
            middlewares: ['AUTH', 'REGULAR'],
        }
    },
    {
        path: '/regular/tasks/create',
        name: 'regular/tasks/create',
        component: CreateTask,
        meta: {
            middlewares: ['AUTH', 'REGULAR'],
        }
    },
    {
        path: '/administrator/tasks',
        name: 'administrator/tasks',
        component: ListTasks,
        meta: {
            middlewares: ['AUTH', 'ADMINISTRATOR'],
        }
    },
    {
        path: '/administrator/tasks/:id',
        name: 'administrator/tasks/details',
        component: ShowTask,
        meta: {
            middlewares: ['AUTH', 'ADMINISTRATOR'],
        }
    }
];

const router = createRouter({
    history: createWebHistory(),
    routes
});

router.beforeEach((to, from) => {
    const authenticated = localStorage.getItem('authenticated');
    const role = localStorage.getItem('role') ?? 'GUEST';

    if (to.meta.middlewares.includes('GUEST') && authenticated) {
        return {
            name: role.toLowerCase() + '/tasks',
        };
    } else if (to.meta.middlewares.includes('AUTH') && !authenticated) {
        return {
            name: 'login',
        };
    }
});

export default router;
